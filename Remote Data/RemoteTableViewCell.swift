//
//  RemoteTableViewCell.swift
//  Remote Data
//
//  Created by Javier Ramon Gil on 12/6/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class RemoteTableViewCell: UITableViewCell {
    @IBOutlet var imageHotel:UIImageView!
    @IBOutlet var titleHotel:UILabel!
    @IBOutlet var spinner: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
