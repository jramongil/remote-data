//
//  RemoteTableViewController.swift
//  Remote Data
//
//  Created by Javier Ramon Gil on 12/6/15.
//  Copyright (c) 2015 Javier Ramon. All rights reserved.
//

import UIKit

class RemoteTableViewController: UITableViewController {
    
    let cellReuseIdentifier = "Reuse Cell"
    var titles: [String]!
    var urls: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.barTintColor = UIColor.redColor()
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        
        self.title = "Remote Data"
        
        let nib = UINib(nibName: "RemoteTableViewCell", bundle: nil)
        tableView.registerNib(nib, forCellReuseIdentifier: cellReuseIdentifier)
        
        let path = NSBundle.mainBundle().pathForResource("Data", ofType: "plist")
        let hotelsArray = NSMutableArray(contentsOfFile: path!)
        titles = hotelsArray?.valueForKey("Title") as! [String]
        urls = hotelsArray?.valueForKey("URL") as! [String]

    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return titles.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! RemoteTableViewCell

        // Configure the cell...
        cell.titleHotel.text = titles[indexPath.row]
        cell.spinner.startAnimating()
        
        let queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        dispatch_async(queue) {
            let group = dispatch_group_create()
            
            dispatch_group_async(group, queue){
            
                let url = NSURL(string: self.urls[indexPath.row])
                let data = NSData(contentsOfURL: url!)
                cell.imageHotel.image = UIImage(data: data!)
            }
            
            dispatch_group_notify(group, queue){
                cell.spinner.stopAnimating()
                cell.spinner.hidesWhenStopped = true
            }
        }
        return cell
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
